# A boilerplate for integrating Concrete5 & Git together

__Summary:__ You can fork or download this repository and use it as a starting point when using Git with Concrete5 websites. 

## What's baked in?

- A `.gitignore` file that has all the goods you don't want tracked with git (because site editors have the ability to change these files and directories--causing your server and Git repo to become out of sync otherwise)
     * This includes ignoring the root `concrete`, `files`, `packages`, `updates`, and `config/site.php` directories and files by default. You can add these directories manually via SFTP or SSH later.
- The basic directory structure
- .gitignore files for all of the appropriate top level directories that need to show up at the root (but are empty by default)

## Some Frequently Asked Questions:

### Why not keep track of the (root)/concrete directory?
Keeping track of the Concrete5 core is probably a bad idea. There are some 14,000 files in the core and tracking it makes everything take longer (`git status`, `add`, `commit`, etc etc). Concrete5 is [already using GitHub](https://github.com/concrete5/concrete5) to version control their development, so it probably doesn't make sense to re-track it yourself. On top of all this, if you're using Git in your deployment workflow, it takes a _suuuper_ long time to deploy--even to change 1 file (roughly 6 minutes to deploy for a readme.md change). Without the (root)/concrete directory, deploys that change multiple files can happen very quickly (less than 30 seconds from my quick tests).

### Concrete5 gives me a permissions error upon install for the `config`, `files`, and `packages` directories

All you need to do here is create these directories if they don't already exist (I only had to create the `files` and `packages` directories). Re-run the tests and everything should give you a green light.

----

This little morsel was crafted with inspiration from [Concrete5's own .gitignore file](https://github.com/concrete5/concrete5/blob/master/.gitignore).